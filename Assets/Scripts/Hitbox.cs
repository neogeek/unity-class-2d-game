﻿using UnityEngine;

public class Hitbox : MonoBehaviour {

    private GameObject player;

    void Awake () {

        player = gameObject.transform.parent.transform.parent.gameObject;

    }

    public void Hit() {

        player.GetComponent<PlayerController>().Die();

    }

}
