﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public bool controllable = true;

    public GameObject groundTrigger;
    public LayerMask groundLayers;

    public GameObject highAttack;

    private Rigidbody2D rb;
    private Animator animator;

    private float maxHorizontalSpeed = 10.0f;
    private float moveHorizontal = 0.0f;
    private bool jumpPressed = false;
    private float jumpForce = 700.0f;

    private bool grounded = false;
    private bool isDead = false;
    private bool isAttacking = false;
    private bool facingRight = true;

    void Awake() {

        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();

    }

    void Update() {

        if (controllable) {

            if (Input.GetButtonDown("Fire1")) {

                StartCoroutine(Attack());

            }

            moveHorizontal = Input.GetAxis("Horizontal");
            jumpPressed = Input.GetKeyDown("space");

        }

    }

    void FixedUpdate() {

        if (!isDead && !isAttacking) {

            grounded = Physics2D.OverlapCircle(groundTrigger.transform.position, 0.2f, groundLayers);
            bool landing = Physics2D.OverlapCircle(groundTrigger.transform.position, 0.5f, groundLayers);

            rb.velocity = new Vector2(moveHorizontal * maxHorizontalSpeed, rb.velocity.y);

            if (jumpPressed && grounded) {

                rb.AddForce(new Vector2(0, jumpForce));

            }

            animator.SetBool("grounded", grounded);

            animator.SetBool("landing", landing);

            animator.SetFloat("speed", Mathf.Abs(moveHorizontal));

            animator.SetFloat("verticalSpeed", rb.velocity.y);

            if ((moveHorizontal > 0 && !facingRight) || (moveHorizontal < 0 && facingRight)) {

                Flip();

            }

        }

    }

    void Flip() {

        Vector3 scale = gameObject.transform.localScale;

        facingRight = !facingRight;

        scale.x = -scale.x;

        gameObject.transform.localScale = scale;

    }

    public IEnumerator Attack() {

        if (grounded && !isDead && !isAttacking) {

            isAttacking = true;

            animator.SetTrigger("attack");

            highAttack.SetActive(true);

            yield return new WaitForSeconds(1.167f);

            highAttack.SetActive(false);

            isAttacking = false;

        }

    }

    public void Die() {

        if (!isDead) {

            isDead = true;

            animator.SetTrigger("dead");

        }

    }

}
